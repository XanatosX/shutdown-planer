﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShutdownTimer.Forms;

namespace ShutdownTimer
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 0)
                CheckArguments(args);
            Language.Initialize();
            Settings.Load();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }

        private static void CheckArguments(string[] Arguments)
        {
            foreach (string argument in Arguments)
            {
                if (argument.StartsWith("-") || argument.StartsWith("/") || argument.StartsWith("\\"))
                {
                    switch (argument.Trim().ToLower().Remove(0,1))
                    {
                        case "debug":
                            Settings.Debug = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
