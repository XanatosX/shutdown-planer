﻿namespace ShutdownTimer.Forms
{
    partial class LanguageSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CB_Languages = new System.Windows.Forms.ComboBox();
            this.L_Language = new System.Windows.Forms.Label();
            this.B_OK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CB_Languages
            // 
            this.CB_Languages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_Languages.FormattingEnabled = true;
            this.CB_Languages.Location = new System.Drawing.Point(85, 12);
            this.CB_Languages.Name = "CB_Languages";
            this.CB_Languages.Size = new System.Drawing.Size(241, 21);
            this.CB_Languages.TabIndex = 0;
            this.CB_Languages.SelectedIndexChanged += new System.EventHandler(this.CB_Languages_SelectedIndexChanged);
            // 
            // L_Language
            // 
            this.L_Language.AutoSize = true;
            this.L_Language.Location = new System.Drawing.Point(12, 15);
            this.L_Language.Name = "L_Language";
            this.L_Language.Size = new System.Drawing.Size(67, 13);
            this.L_Language.TabIndex = 1;
            this.L_Language.Text = "L_Language";
            // 
            // B_OK
            // 
            this.B_OK.AutoSize = true;
            this.B_OK.Location = new System.Drawing.Point(15, 39);
            this.B_OK.Name = "B_OK";
            this.B_OK.Size = new System.Drawing.Size(75, 23);
            this.B_OK.TabIndex = 2;
            this.B_OK.Text = "B_OK";
            this.B_OK.UseVisualStyleBackColor = true;
            this.B_OK.Click += new System.EventHandler(this.B_OK_Click);
            // 
            // LanguageSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 73);
            this.Controls.Add(this.B_OK);
            this.Controls.Add(this.L_Language);
            this.Controls.Add(this.CB_Languages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LanguageSelection";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LanguageSelection";
            this.Load += new System.EventHandler(this.LanguageSelection_Load);
            this.Shown += new System.EventHandler(this.LanguageSelection_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CB_Languages;
        private System.Windows.Forms.Label L_Language;
        private System.Windows.Forms.Button B_OK;
    }
}