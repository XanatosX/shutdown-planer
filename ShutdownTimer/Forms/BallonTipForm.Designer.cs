﻿namespace ShutdownTimer
{
    partial class BallonTipForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_Title = new System.Windows.Forms.TextBox();
            this.TB_Body = new System.Windows.Forms.TextBox();
            this.TB_WarnAtPercent = new System.Windows.Forms.TextBox();
            this.TB_SoundFile = new System.Windows.Forms.TextBox();
            this.L_Title = new System.Windows.Forms.Label();
            this.L_Body = new System.Windows.Forms.Label();
            this.L_WarnAtPercent = new System.Windows.Forms.Label();
            this.L_SoundFile = new System.Windows.Forms.Label();
            this.B_OpenFile = new System.Windows.Forms.Button();
            this.B_Save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TB_Title
            // 
            this.TB_Title.Location = new System.Drawing.Point(110, 6);
            this.TB_Title.Name = "TB_Title";
            this.TB_Title.Size = new System.Drawing.Size(171, 20);
            this.TB_Title.TabIndex = 0;
            // 
            // TB_Body
            // 
            this.TB_Body.Location = new System.Drawing.Point(110, 32);
            this.TB_Body.Name = "TB_Body";
            this.TB_Body.Size = new System.Drawing.Size(171, 20);
            this.TB_Body.TabIndex = 1;
            // 
            // TB_WarnAtPercent
            // 
            this.TB_WarnAtPercent.Location = new System.Drawing.Point(110, 58);
            this.TB_WarnAtPercent.Name = "TB_WarnAtPercent";
            this.TB_WarnAtPercent.Size = new System.Drawing.Size(171, 20);
            this.TB_WarnAtPercent.TabIndex = 2;
            this.TB_WarnAtPercent.TextChanged += new System.EventHandler(this.TB_WarnAtPercent_TextChanged);
            // 
            // TB_SoundFile
            // 
            this.TB_SoundFile.Location = new System.Drawing.Point(110, 84);
            this.TB_SoundFile.Name = "TB_SoundFile";
            this.TB_SoundFile.Size = new System.Drawing.Size(171, 20);
            this.TB_SoundFile.TabIndex = 3;
            // 
            // L_Title
            // 
            this.L_Title.AutoSize = true;
            this.L_Title.Location = new System.Drawing.Point(12, 9);
            this.L_Title.Name = "L_Title";
            this.L_Title.Size = new System.Drawing.Size(39, 13);
            this.L_Title.TabIndex = 4;
            this.L_Title.Text = "L_Title";
            // 
            // L_Body
            // 
            this.L_Body.AutoSize = true;
            this.L_Body.Location = new System.Drawing.Point(12, 35);
            this.L_Body.Name = "L_Body";
            this.L_Body.Size = new System.Drawing.Size(43, 13);
            this.L_Body.TabIndex = 5;
            this.L_Body.Text = "L_Body";
            // 
            // L_WarnAtPercent
            // 
            this.L_WarnAtPercent.AutoSize = true;
            this.L_WarnAtPercent.Location = new System.Drawing.Point(12, 61);
            this.L_WarnAtPercent.Name = "L_WarnAtPercent";
            this.L_WarnAtPercent.Size = new System.Drawing.Size(92, 13);
            this.L_WarnAtPercent.TabIndex = 6;
            this.L_WarnAtPercent.Text = "L_WarnAtPercent";
            // 
            // L_SoundFile
            // 
            this.L_SoundFile.AutoSize = true;
            this.L_SoundFile.Location = new System.Drawing.Point(12, 87);
            this.L_SoundFile.Name = "L_SoundFile";
            this.L_SoundFile.Size = new System.Drawing.Size(66, 13);
            this.L_SoundFile.TabIndex = 7;
            this.L_SoundFile.Text = "L_SoundFile";
            // 
            // B_OpenFile
            // 
            this.B_OpenFile.Location = new System.Drawing.Point(206, 110);
            this.B_OpenFile.Name = "B_OpenFile";
            this.B_OpenFile.Size = new System.Drawing.Size(75, 23);
            this.B_OpenFile.TabIndex = 8;
            this.B_OpenFile.Text = "B_OpenFile";
            this.B_OpenFile.UseVisualStyleBackColor = true;
            this.B_OpenFile.Visible = false;
            // 
            // B_Save
            // 
            this.B_Save.Location = new System.Drawing.Point(12, 110);
            this.B_Save.Name = "B_Save";
            this.B_Save.Size = new System.Drawing.Size(75, 23);
            this.B_Save.TabIndex = 9;
            this.B_Save.Text = "B_Save";
            this.B_Save.UseVisualStyleBackColor = true;
            this.B_Save.Click += new System.EventHandler(this.B_Save_Click);
            // 
            // BallonTipForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 153);
            this.Controls.Add(this.B_Save);
            this.Controls.Add(this.B_OpenFile);
            this.Controls.Add(this.L_SoundFile);
            this.Controls.Add(this.L_WarnAtPercent);
            this.Controls.Add(this.L_Body);
            this.Controls.Add(this.L_Title);
            this.Controls.Add(this.TB_SoundFile);
            this.Controls.Add(this.TB_WarnAtPercent);
            this.Controls.Add(this.TB_Body);
            this.Controls.Add(this.TB_Title);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "BallonTipForm";
            this.Text = "BallonTipForm";
            this.Load += new System.EventHandler(this.BallonTipForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TB_Title;
        private System.Windows.Forms.TextBox TB_Body;
        private System.Windows.Forms.TextBox TB_WarnAtPercent;
        private System.Windows.Forms.TextBox TB_SoundFile;
        private System.Windows.Forms.Label L_Title;
        private System.Windows.Forms.Label L_Body;
        private System.Windows.Forms.Label L_WarnAtPercent;
        private System.Windows.Forms.Label L_SoundFile;
        private System.Windows.Forms.Button B_OpenFile;
        private System.Windows.Forms.Button B_Save;
    }
}