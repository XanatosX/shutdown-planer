﻿namespace ShutdownTimer.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MS_Main = new System.Windows.Forms.MenuStrip();
            this.MSI_File = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Language = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMI_Hide = new System.Windows.Forms.ToolStripMenuItem();
            this.B_Control = new System.Windows.Forms.Button();
            this.B_Close = new System.Windows.Forms.Button();
            this.T_CheckTimer = new System.Windows.Forms.Timer(this.components);
            this.CB_Hour = new System.Windows.Forms.ComboBox();
            this.CB_Minute = new System.Windows.Forms.ComboBox();
            this.CB_Second = new System.Windows.Forms.ComboBox();
            this.GB_TimeFromNow = new System.Windows.Forms.GroupBox();
            this.L_Second = new System.Windows.Forms.Label();
            this.L_Minute = new System.Windows.Forms.Label();
            this.L_Hour = new System.Windows.Forms.Label();
            this.L_ShutDownCountdown = new System.Windows.Forms.Label();
            this.L_EndTime = new System.Windows.Forms.Label();
            this.StS_Main = new System.Windows.Forms.StatusStrip();
            this.TSSL_FILLER = new System.Windows.Forms.ToolStripStatusLabel();
            this.TSSL_Version = new System.Windows.Forms.ToolStripStatusLabel();
            this.NI_MainApp = new System.Windows.Forms.NotifyIcon(this.components);
            this.CMS_Main = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMS_ShowApp = new System.Windows.Forms.ToolStripMenuItem();
            this.CMS_StopApp = new System.Windows.Forms.ToolStripMenuItem();
            this.MS_Main.SuspendLayout();
            this.GB_TimeFromNow.SuspendLayout();
            this.StS_Main.SuspendLayout();
            this.CMS_Main.SuspendLayout();
            this.SuspendLayout();
            // 
            // MS_Main
            // 
            this.MS_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MSI_File});
            this.MS_Main.Location = new System.Drawing.Point(0, 0);
            this.MS_Main.Name = "MS_Main";
            this.MS_Main.Size = new System.Drawing.Size(249, 24);
            this.MS_Main.TabIndex = 0;
            this.MS_Main.Text = "menuStrip1";
            // 
            // MSI_File
            // 
            this.MSI_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMI_Language,
            this.TSMI_Settings,
            this.TSMI_Hide});
            this.MSI_File.Name = "MSI_File";
            this.MSI_File.Size = new System.Drawing.Size(62, 20);
            this.MSI_File.Text = "MSI_File";
            // 
            // TSMI_Language
            // 
            this.TSMI_Language.Name = "TSMI_Language";
            this.TSMI_Language.Size = new System.Drawing.Size(158, 22);
            this.TSMI_Language.Text = "TSMI_Language";
            this.TSMI_Language.Click += new System.EventHandler(this.TSMI_Language_Click);
            // 
            // TSMI_Settings
            // 
            this.TSMI_Settings.Name = "TSMI_Settings";
            this.TSMI_Settings.Size = new System.Drawing.Size(158, 22);
            this.TSMI_Settings.Text = "TSMI_Settings";
            this.TSMI_Settings.Click += new System.EventHandler(this.TSMI_Settings_Click);
            // 
            // TSMI_Hide
            // 
            this.TSMI_Hide.Name = "TSMI_Hide";
            this.TSMI_Hide.Size = new System.Drawing.Size(158, 22);
            this.TSMI_Hide.Text = "TSMI_Hide";
            this.TSMI_Hide.Click += new System.EventHandler(this.TSMI_Hide_Click);
            // 
            // B_Control
            // 
            this.B_Control.AutoSize = true;
            this.B_Control.Location = new System.Drawing.Point(12, 171);
            this.B_Control.Name = "B_Control";
            this.B_Control.Size = new System.Drawing.Size(75, 23);
            this.B_Control.TabIndex = 1;
            this.B_Control.Text = "B_Control";
            this.B_Control.UseVisualStyleBackColor = true;
            this.B_Control.Click += new System.EventHandler(this.B_Control_Click);
            // 
            // B_Close
            // 
            this.B_Close.AutoSize = true;
            this.B_Close.Location = new System.Drawing.Point(154, 171);
            this.B_Close.Name = "B_Close";
            this.B_Close.Size = new System.Drawing.Size(75, 23);
            this.B_Close.TabIndex = 2;
            this.B_Close.Text = "B_Close";
            this.B_Close.UseVisualStyleBackColor = true;
            this.B_Close.Click += new System.EventHandler(this.B_Close_Click);
            // 
            // T_CheckTimer
            // 
            this.T_CheckTimer.Interval = 1000;
            this.T_CheckTimer.Tick += new System.EventHandler(this.T_CheckTimer_Tick);
            // 
            // CB_Hour
            // 
            this.CB_Hour.FormattingEnabled = true;
            this.CB_Hour.Location = new System.Drawing.Point(68, 19);
            this.CB_Hour.Name = "CB_Hour";
            this.CB_Hour.Size = new System.Drawing.Size(77, 21);
            this.CB_Hour.TabIndex = 4;
            // 
            // CB_Minute
            // 
            this.CB_Minute.FormattingEnabled = true;
            this.CB_Minute.Location = new System.Drawing.Point(68, 46);
            this.CB_Minute.Name = "CB_Minute";
            this.CB_Minute.Size = new System.Drawing.Size(77, 21);
            this.CB_Minute.TabIndex = 5;
            // 
            // CB_Second
            // 
            this.CB_Second.FormattingEnabled = true;
            this.CB_Second.Location = new System.Drawing.Point(68, 73);
            this.CB_Second.Name = "CB_Second";
            this.CB_Second.Size = new System.Drawing.Size(77, 21);
            this.CB_Second.TabIndex = 6;
            // 
            // GB_TimeFromNow
            // 
            this.GB_TimeFromNow.Controls.Add(this.L_Second);
            this.GB_TimeFromNow.Controls.Add(this.L_Minute);
            this.GB_TimeFromNow.Controls.Add(this.L_Hour);
            this.GB_TimeFromNow.Controls.Add(this.CB_Minute);
            this.GB_TimeFromNow.Controls.Add(this.CB_Hour);
            this.GB_TimeFromNow.Controls.Add(this.CB_Second);
            this.GB_TimeFromNow.Location = new System.Drawing.Point(12, 65);
            this.GB_TimeFromNow.Name = "GB_TimeFromNow";
            this.GB_TimeFromNow.Size = new System.Drawing.Size(217, 100);
            this.GB_TimeFromNow.TabIndex = 7;
            this.GB_TimeFromNow.TabStop = false;
            this.GB_TimeFromNow.Text = "GB_TimeFromNow";
            // 
            // L_Second
            // 
            this.L_Second.AutoSize = true;
            this.L_Second.Location = new System.Drawing.Point(6, 76);
            this.L_Second.Name = "L_Second";
            this.L_Second.Size = new System.Drawing.Size(56, 13);
            this.L_Second.TabIndex = 9;
            this.L_Second.Text = "L_Second";
            // 
            // L_Minute
            // 
            this.L_Minute.AutoSize = true;
            this.L_Minute.Location = new System.Drawing.Point(6, 49);
            this.L_Minute.Name = "L_Minute";
            this.L_Minute.Size = new System.Drawing.Size(51, 13);
            this.L_Minute.TabIndex = 8;
            this.L_Minute.Text = "L_Minute";
            // 
            // L_Hour
            // 
            this.L_Hour.AutoSize = true;
            this.L_Hour.Location = new System.Drawing.Point(6, 22);
            this.L_Hour.Name = "L_Hour";
            this.L_Hour.Size = new System.Drawing.Size(42, 13);
            this.L_Hour.TabIndex = 7;
            this.L_Hour.Text = "L_Hour";
            // 
            // L_ShutDownCountdown
            // 
            this.L_ShutDownCountdown.AutoSize = true;
            this.L_ShutDownCountdown.Location = new System.Drawing.Point(12, 24);
            this.L_ShutDownCountdown.Name = "L_ShutDownCountdown";
            this.L_ShutDownCountdown.Size = new System.Drawing.Size(123, 13);
            this.L_ShutDownCountdown.TabIndex = 8;
            this.L_ShutDownCountdown.Text = "L_ShutDownCountdown";
            // 
            // L_EndTime
            // 
            this.L_EndTime.AutoSize = true;
            this.L_EndTime.Location = new System.Drawing.Point(13, 45);
            this.L_EndTime.Name = "L_EndTime";
            this.L_EndTime.Size = new System.Drawing.Size(61, 13);
            this.L_EndTime.TabIndex = 9;
            this.L_EndTime.Text = "L_EndTime";
            // 
            // StS_Main
            // 
            this.StS_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSSL_FILLER,
            this.TSSL_Version});
            this.StS_Main.Location = new System.Drawing.Point(0, 204);
            this.StS_Main.Name = "StS_Main";
            this.StS_Main.Size = new System.Drawing.Size(249, 22);
            this.StS_Main.SizingGrip = false;
            this.StS_Main.TabIndex = 10;
            this.StS_Main.Text = "StS_Main";
            // 
            // TSSL_FILLER
            // 
            this.TSSL_FILLER.Name = "TSSL_FILLER";
            this.TSSL_FILLER.Size = new System.Drawing.Size(158, 17);
            this.TSSL_FILLER.Spring = true;
            this.TSSL_FILLER.Text = "TSSL_FILLER";
            // 
            // TSSL_Version
            // 
            this.TSSL_Version.Name = "TSSL_Version";
            this.TSSL_Version.Size = new System.Drawing.Size(76, 17);
            this.TSSL_Version.Text = "TSSL_Version";
            // 
            // NI_MainApp
            // 
            this.NI_MainApp.ContextMenuStrip = this.CMS_Main;
            this.NI_MainApp.Icon = ((System.Drawing.Icon)(resources.GetObject("NI_MainApp.Icon")));
            this.NI_MainApp.Text = "APP_Name";
            this.NI_MainApp.Visible = true;
            this.NI_MainApp.Click += new System.EventHandler(this.NI_MainApp_Click);
            // 
            // CMS_Main
            // 
            this.CMS_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMS_ShowApp,
            this.CMS_StopApp});
            this.CMS_Main.Name = "CMS_Main";
            this.CMS_Main.Size = new System.Drawing.Size(156, 48);
            // 
            // CMS_ShowApp
            // 
            this.CMS_ShowApp.Enabled = false;
            this.CMS_ShowApp.Name = "CMS_ShowApp";
            this.CMS_ShowApp.Size = new System.Drawing.Size(155, 22);
            this.CMS_ShowApp.Text = "CMS_ShowApp";
            this.CMS_ShowApp.Click += new System.EventHandler(this.cMSShowAppToolStripMenuItem_Click);
            // 
            // CMS_StopApp
            // 
            this.CMS_StopApp.Enabled = false;
            this.CMS_StopApp.Name = "CMS_StopApp";
            this.CMS_StopApp.Size = new System.Drawing.Size(155, 22);
            this.CMS_StopApp.Text = "CMS_StopApp";
            this.CMS_StopApp.Click += new System.EventHandler(this.CMS_StopApp_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(249, 226);
            this.Controls.Add(this.StS_Main);
            this.Controls.Add(this.L_EndTime);
            this.Controls.Add(this.L_ShutDownCountdown);
            this.Controls.Add(this.GB_TimeFromNow);
            this.Controls.Add(this.B_Close);
            this.Controls.Add(this.B_Control);
            this.Controls.Add(this.MS_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MS_Main;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MS_Main.ResumeLayout(false);
            this.MS_Main.PerformLayout();
            this.GB_TimeFromNow.ResumeLayout(false);
            this.GB_TimeFromNow.PerformLayout();
            this.StS_Main.ResumeLayout(false);
            this.StS_Main.PerformLayout();
            this.CMS_Main.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MS_Main;
        private System.Windows.Forms.ToolStripMenuItem MSI_File;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Language;
        private System.Windows.Forms.Button B_Control;
        private System.Windows.Forms.Button B_Close;
        private System.Windows.Forms.Timer T_CheckTimer;
        private System.Windows.Forms.ComboBox CB_Hour;
        private System.Windows.Forms.ComboBox CB_Minute;
        private System.Windows.Forms.ComboBox CB_Second;
        private System.Windows.Forms.GroupBox GB_TimeFromNow;
        private System.Windows.Forms.Label L_Second;
        private System.Windows.Forms.Label L_Minute;
        private System.Windows.Forms.Label L_Hour;
        private System.Windows.Forms.Label L_ShutDownCountdown;
        private System.Windows.Forms.Label L_EndTime;
        private System.Windows.Forms.StatusStrip StS_Main;
        private System.Windows.Forms.ToolStripStatusLabel TSSL_FILLER;
        private System.Windows.Forms.ToolStripStatusLabel TSSL_Version;
        private System.Windows.Forms.NotifyIcon NI_MainApp;
        private System.Windows.Forms.ContextMenuStrip CMS_Main;
        private System.Windows.Forms.ToolStripMenuItem CMS_ShowApp;
        private System.Windows.Forms.ToolStripMenuItem CMS_StopApp;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Hide;
        private System.Windows.Forms.ToolStripMenuItem TSMI_Settings;
    }
}