﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShutdownTimer.Forms
{
    public partial class MainForm : Form
    {

        private int Distance;
        private bool NotificationActive;
        private DateTime EndTime;
        private DateTime StartTime;

        List<BallonTipWarning> ActiveWarningList;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!Settings.LanguageLoadet)
            {
                LanguageSelection LS = new LanguageSelection();
                LS.ShowDialog();
            }
            B_Control.Tag = "start";
            Control LargestLabel = GetLargestLabelInGroupBox(GB_TimeFromNow);
            int Distance = CB_Hour.Location.X - (LargestLabel.Location.X + LargestLabel.Width);
            SetupLanguage();
            FillComboBox();
        }

        private void SetupLanguage()
        {
            this.Text = Language.GetString("MainForm_Title");
            if (Settings.Debug)
                this.Text += " - Debug";
            NI_MainApp.Text = Language.GetString("MainForm_Title");
            CMS_ShowApp.Text = Language.GetString("MainForm_ShowForm");
            CMS_StopApp.Text = Language.GetString("Default_Stop");
            MSI_File.Text = Language.GetString("MainForm_File");
            TSMI_Language.Text = Language.GetString("LanguageSelection_Title");
            B_Close.Text = Language.GetString("Default_Close");
            if (B_Control.Tag.ToString().ToLower() == "start")
            {
                B_Control.Text = Language.GetString("Default_Start");
            }
            else if (B_Control.Tag.ToString().ToLower() == "stop")
            {
                B_Control.Text = Language.GetString("Default_Stop");
            }
            L_Hour.Text = Language.GetString("MainForm_Hour");
            L_Minute.Text = Language.GetString("MainForm_Minute");
            L_Second.Text = Language.GetString("MainForm_Second");
            GB_TimeFromNow.Text = Language.GetString("MainForm_GB_TimeFromNow");
            if (!T_CheckTimer.Enabled)
            {
                L_ShutDownCountdown.Text = Language.GetString("Default_NoTimer");
                L_EndTime.Text = Language.GetString("Default_Empty");
            }
            else
            {
                SetTimeLeft();
                L_EndTime.Text = String.Format(Language.GetString("MainForm_EndTime"), EndTime.ToString("T"));
            }
            TSSL_FILLER.Text = Language.GetString("Default_Empty");
            if (!String.IsNullOrEmpty(Settings.Version))  
                TSSL_Version.Text = String.Format(Language.GetString("MainForm_Version"), Settings.Version);
            DesignGroupBox();
            TSMI_Hide.Text = Language.GetString("MainForm_Hide");
            TSMI_Settings.Text = Language.GetString("Default_Settings");
        }

        private void FillComboBox()
        {
            for (int i = 0; i < 25; i++)
            {
                CB_Hour.Items.Add(i);
            }
            for (int i = 0; i < 61; i++)
            {
                CB_Minute.Items.Add(i);
                CB_Second.Items.Add(i);
            }
            CB_Hour.SelectedIndex = 0;
            CB_Minute.SelectedIndex = 0;
            CB_Second.SelectedIndex = 0;
        }

        private void DesignGroupBox()
        {
            Control LargestLabel = GetLargestLabelInGroupBox(GB_TimeFromNow);
            SetXForAllControlsOfType(typeof(ComboBox), GB_TimeFromNow, LargestLabel.Location.X + LargestLabel.Width + Distance);
        }

        private Control GetLargestLabelInGroupBox(GroupBox Box)
        {
            int MaxHWidth = int.MinValue;
            Control ReturnControl = null;
            foreach (Control control in Box.Controls)
            {
                if (control.GetType() != typeof(Label))
                    continue;
                if (control.Width > MaxHWidth)
                {
                    ReturnControl = control;
                    MaxHWidth = control.Width;
                }
            }
            return ReturnControl;
        }

        private void SetXForAllControlsOfType(Type ControlType, Control Parent, int NewXPos)
        {
            foreach (Control control in Parent.Controls)
            {
                if (control.GetType() == ControlType)
                {
                    control.Location = new Point(NewXPos, control.Location.Y);
                }
            }
        }

        private void TSMI_Language_Click(object sender, EventArgs e)
        {
            LanguageSelection LS = new LanguageSelection();
            LS.ShowDialog();
            SetupLanguage();
        }

        private void B_Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void B_Control_Click(object sender, EventArgs e)
        {
            if (B_Control.Tag.ToString() == "start")
            {
                GB_TimeFromNow.Enabled = false;
                CMS_StopApp.Enabled = true;
                B_Control.Tag = "stop";
                TSMI_Language.Enabled = false;
                TSMI_Settings.Enabled = false;
                B_Control.Text = Language.GetString("Default_Stop");
                DateTime CurrentTime = DateTime.Now;
                EndTime = CurrentTime + new TimeSpan(int.Parse(CB_Hour.Text), int.Parse(CB_Minute.Text), int.Parse(CB_Second.Text));
                StartTime = CurrentTime;
                L_EndTime.Text = String.Format(Language.GetString("MainForm_EndTime"), EndTime.ToString("T"));
                T_CheckTimer.Start();
                SetTimeLeft();
                TimeSpan MinTimeSpan = new TimeSpan(0, 0, Settings.MinTimeForNotifications);
                DateTime MinDateTime = CurrentTime + MinTimeSpan;
                if (EndTime >= MinDateTime)
                {
                    NotificationActive = true;
                    ActiveWarningList = new List<BallonTipWarning>();
                    foreach (BallonTipWarning warnings in Settings.Warnings.ToArray().ToList())
                    {
                        if (warnings.Active)
                            ActiveWarningList.Add(warnings);
                    }

                    SortWarningList();

                }
                else
                    NotificationActive = false;

                if (Settings.HideAfterStart)
                {
                    this.Hide();
                    CMS_ShowApp.Enabled = true;
                }
            }
            else
            {
                StopTimer();
            }
        }

        private void SortWarningList()
        {
            if (ActiveWarningList.Count > 0)
            {
                ActiveWarningList.Sort(delegate (BallonTipWarning O1, BallonTipWarning O2) { return O1.WarnAtProcent.CompareTo(O2.WarnAtProcent); });
            }
        }

        private void StopTimer()
        {
            GB_TimeFromNow.Enabled = true;
            CMS_StopApp.Enabled = false;
            B_Control.Tag = "start";
            TSMI_Language.Enabled = true;
            TSMI_Settings.Enabled = true;
            B_Control.Text = Language.GetString("Default_Start");
            L_EndTime.Text = Language.GetString("Default_Empty");
            L_ShutDownCountdown.Text = Language.GetString("Default_NoTimer");
            T_CheckTimer.Stop();
        }

        private void T_CheckTimer_Tick(object sender, EventArgs e)
        {
            if (EndTime < DateTime.Now)
            {
                T_CheckTimer.Stop();
                B_Control.Tag = "stop";
                B_Control.PerformClick();

                if (Settings.Debug)
                {
                    MessageBox.Show("Alarm Trigger");
                    return;
                }
                ShutdownComputer();
                return;
            }
            if (NotificationActive)
                CheckIfNotificationIsNecessary();
            SetTimeLeft();
        }
        private void ShutdownComputer()
        {
            string ShutdownCommand = "/s /t 0";
            if (Settings.ForceShutdown)
            {
                ShutdownCommand += " /f";
            }
            ProcessStartInfo PSI = new ProcessStartInfo("shutdown");
            PSI.UseShellExecute = false;
            PSI.RedirectStandardOutput = true;
            PSI.Arguments = ShutdownCommand;
            PSI.CreateNoWindow = true;
            PSI.UseShellExecute = false;
            Process Shutdown = new Process();
            Shutdown.StartInfo = PSI;
            Shutdown.Start();
        }

        private void CheckIfNotificationIsNecessary()
        {
            TimeSpan TimeLeft = EndTime - DateTime.Now;
            TimeSpan TotalTime = EndTime - StartTime;
            float PercentDone = (float)Math.Round(100 - (TimeLeft.TotalMilliseconds / TotalTime.TotalMilliseconds) * 100, 2);
            if (ActiveWarningList.Count > 0)
            {
                BallonTipWarning CurrentWarning;
                int Index = GetNextWarning(out CurrentWarning);
                if (CurrentWarning.WarnAtProcent <= PercentDone)
                {
                    NI_MainApp.ShowBalloonTip(20, CurrentWarning.Title, CurrentWarning.Text, CurrentWarning.Icon);
                    if (Index < 0)
                        ActiveWarningList.Clear();
                    ActiveWarningList.RemoveAt(Index);
                    if (CurrentWarning.PlaySound)
                        CurrentWarning.Play();
                }
            }
        }

        private int GetNextWarning(out BallonTipWarning CurrentBallonTip)
        {
            int CurrentPosition = -1;
            int CurrentWarning = int.MaxValue;
            CurrentBallonTip = null;
            for (int i = 0; i < ActiveWarningList.Count; i++)
            {
                CurrentBallonTip = ActiveWarningList[i];
                if (CurrentBallonTip.WarnAtProcent <= CurrentWarning)
                {
                    CurrentWarning = CurrentBallonTip.WarnAtProcent;
                    CurrentPosition = i;
                }
            }
            CurrentBallonTip = ActiveWarningList[CurrentPosition];
            return CurrentPosition;
        }

        private void SetTimeLeft()
        {
            TimeSpan TimeDifference = EndTime - DateTime.Now;
            L_ShutDownCountdown.Text = String.Format(Language.GetString("MainForm_Timer"), TimeDifference.Hours.ToString("00"), TimeDifference.Minutes.ToString("00"), TimeDifference.Seconds.ToString("00"));
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Save();
        }

        private void CMS_StopApp_Click(object sender, EventArgs e)
        {
            StopTimer();
        }

        private void cMSShowAppToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeAppVisability(false);
        }

        private void TSMI_Hide_Click(object sender, EventArgs e)
        {
            ChangeAppVisability(true);
        }

        private void ChangeAppVisability(bool Hide)
        {
            if (Hide)
                this.Hide();
            else
                this.Show();

            CMS_ShowApp.Enabled = Hide;
        }

        private void TSMI_Settings_Click(object sender, EventArgs e)
        {
            SettingsForm SF = new SettingsForm();
            SF.ShowDialog();
        }

        private void NI_MainApp_Click(object sender, EventArgs e)
        {
            Show();
            TopMost = true;
            TopMost = false;
        }
    }
}
