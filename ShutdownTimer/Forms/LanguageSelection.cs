﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShutdownTimer.Forms
{
    public partial class LanguageSelection : Form
    {
        private bool Startup = true;

        public LanguageSelection()
        {
            InitializeComponent();
        }

        private void LanguageSelection_Load(object sender, EventArgs e)
        {
            LoadLanguage();
            SetupDropDown();

        }

        private void SetupDropDown()
        {
            CB_Languages.Items.Clear();
            foreach (CultureInfo Culture in Language.GetCultures())
            {
                CB_Languages.Items.Add(Culture.DisplayName);
            }
            TryToSelectCurrentLanguage();
        }

        private bool TryToSelectCurrentLanguage()
        {
            for (int i = 0; i < CB_Languages.Items.Count; i++)
            {
                if (CB_Languages.Items[i].ToString() == Language.CurrentCulturename)
                {
                    CB_Languages.SelectedIndex = i;
                    return true;
                }
            }
            if (CB_Languages.Items.Count > 0)
            {
                CB_Languages.SelectedIndex = 0;
                return true;
            }
            return false;
        }

        private CultureInfo GetCultureByDisplayName(string Name)
        {
            foreach (CultureInfo Culture in Language.GetCultures())
            {
                if (Culture.DisplayName == Name)
                    return Culture;
            }
            return null;
        }

        private void LoadLanguage()
        {
            B_OK.Text = Language.GetString("Default_OK");
            L_Language.Text = Language.GetString("LanguageSelection_Language");
            this.Text = Language.GetString("LanguageSelection_Title");
            B_OK.CenterControlHorizontal(this);
        }

        private void CB_Languages_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Startup)
                return;
            CultureInfo NewCulture = GetCultureByDisplayName(CB_Languages.Text);
            Language.switch_language(NewCulture);
            LoadLanguage();
        }

        private void B_OK_Click(object sender, EventArgs e)
        {
            Settings.Save();
            this.Close();
        }

        private void LanguageSelection_Shown(object sender, EventArgs e)
        {
            Startup = false;
        }
    }
}
