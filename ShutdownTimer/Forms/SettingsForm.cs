﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ShutdownTimer.Forms
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void LoadLanguage()
        {
            CB_HideAfterStart.Text = Language.GetString("SettingsForm_HideAfterStart");
            B_Close.Text = Language.GetString("Default_Close");
            B_SaveAndClose.Text = Language.GetString("SettingsForm_SaveClose");
            TP_Common.Text = Language.GetString("SettingsForm_CommonTab");
            TP_BallonTip.Text = Language.GetString("SettingsForm_BallonTipTab");
            CB_ForceClose.Text = Language.GetString("SettingsForm_ForceClose");
            CMSI_Delete.Text = Language.GetString("SettingsForm_CSMI_Delete");

            CH_Title.Text = Language.GetString("SettingsForms_CH_Title");
            CH_PlaySound.Text = Language.GetString("SettingsForms_CH_PlaySound");
            CH_WarnAtPercent.Text = Language.GetString("SettingsForms_CH_WarnAtPercent");
            CH_Active.Text = Language.GetString("Default_Empty");
      
            this.Text = Language.GetString("Default_Settings");
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            CB_HideAfterStart.Checked = Settings.HideAfterStart;
            ImportBallonTips();

            LoadLanguage();
        }

        private void ImportBallonTips()
        {
            LV_BallonTips.Items.Clear();
            foreach (BallonTipWarning CurrentBallontip in Settings.Warnings)
            {

                ListViewItem NewLVI = new ListViewItem();
                NewLVI.SubItems.Add(CurrentBallontip.Title);
                NewLVI.SubItems.Add(CurrentBallontip.WarnAtProcent.ToString());

                NewLVI.SubItems.Add(BoolToString(CurrentBallontip.PlaySound));
                NewLVI.Checked = CurrentBallontip.Active;
                NewLVI.Tag = CurrentBallontip;
                LV_BallonTips.Items.Add(NewLVI);
            }
            LV_BallonTips.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            LV_BallonTips.AutoResizeColumn(0, ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        private string BoolToString(bool BoolToCheck)
        {
            if (BoolToCheck)
                return Language.GetString("Default_Yes");
            else
                return Language.GetString("Default_No");
        }

        private void SaveBallonTips()
        {
            Settings.Warnings.Clear();
            foreach (ListViewItem LVI in LV_BallonTips.Items)
            {
                if (LVI.Tag.GetType() == typeof(BallonTipWarning))
                {
                    BallonTipWarning CurrentTip = (BallonTipWarning)LVI.Tag;
                    CurrentTip.Active = LVI.Checked;
                    Settings.Warnings.Add(CurrentTip);
                }
            }
        }


        private void SaveSettings()
        {
            Settings.HideAfterStart = CB_HideAfterStart.Checked;
            Settings.ForceShutdown = CB_ForceClose.Checked;
            SaveBallonTips();
            Settings.Save();
        }

        private void B_SaveAndClose_Click(object sender, EventArgs e)
        {
            SaveSettings();
            CloseForm();
        }

        private void CloseForm()
        {
            this.Close();
        }

        private void B_Close_Click(object sender, EventArgs e)
        {
            CloseForm();
        }

        private void CMSI_Delete_Click(object sender, EventArgs e)
        {
            if (LV_BallonTips.SelectedItems.Count == 1 )
            {
                LV_BallonTips.Items.Remove(LV_BallonTips.SelectedItems[0]);
            }
        }

        private void CMS_BallonTips_Opening(object sender, CancelEventArgs e)
        {
            if (LV_BallonTips.SelectedItems.Count > 0)
            {
                CMSI_AddEdit.Text = Language.GetString("SettingsForm_CSMI_Edit");
                CMSI_AddEdit.Tag = false;
                CMSI_Delete.Enabled = true;
            }
            else
            {
                CMSI_AddEdit.Text = Language.GetString("SettingsForm_CSMI_Add");
                CMSI_AddEdit.Tag = true;
                CMSI_Delete.Enabled = false;
            }
        }

        private void CMSI_AddEdit_Click(object sender, EventArgs e)
        {
            if (CMSI_AddEdit.Tag.GetType() == typeof(bool))
            {
                BallonTipWarning CurrentWarning = null;
                ListViewItem CurrentItem = null;
                bool Add = (bool)CMSI_AddEdit.Tag;
                BallonTipForm BTF;
                if (Add)
                    BTF = new BallonTipForm();
                else
                {
                    object ItemTag = LV_BallonTips.SelectedItems[0].Tag;
                    CurrentItem = LV_BallonTips.SelectedItems[0];
                    if (ItemTag.GetType() == typeof(BallonTipWarning))
                        CurrentWarning = (BallonTipWarning)ItemTag;
                    BTF = new BallonTipForm(CurrentWarning);
                }
                BTF.ShowDialog();
                BallonTipWarning NewWarning = BTF.CurrentWarning;
                if (NewWarning == null)
                    return;
                if (Add)
                {
                    //Settings.Warnings.Add(NewWarning);
                    ListViewItem NewLVI = new ListViewItem();
                    NewLVI.SubItems.Add(NewWarning.Title);
                    NewLVI.SubItems.Add(NewWarning.WarnAtProcent.ToString());

                    NewLVI.SubItems.Add(BoolToString(NewWarning.PlaySound));
                    NewLVI.Checked = NewWarning.Active;
                    NewLVI.Tag = NewWarning;
                    LV_BallonTips.Items.Add(NewLVI);
                }
                else
                {
                    if (CurrentWarning != null && CurrentItem != null)
                    {
                        CurrentItem.SubItems[1].Text = NewWarning.Title;
                        CurrentItem.SubItems[2].Text = NewWarning.WarnAtProcent.ToString();
                        CurrentItem.SubItems[3].Text = BoolToString(NewWarning.PlaySound);
                        CurrentItem.Tag = NewWarning;
                    }
                }
                //ImportBallonTips();

            }
        }
    }
}
