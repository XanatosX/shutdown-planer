﻿namespace ShutdownTimer.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CB_HideAfterStart = new System.Windows.Forms.CheckBox();
            this.B_SaveAndClose = new System.Windows.Forms.Button();
            this.B_Close = new System.Windows.Forms.Button();
            this.CB_ForceClose = new System.Windows.Forms.CheckBox();
            this.TC_Settings = new System.Windows.Forms.TabControl();
            this.TP_Common = new System.Windows.Forms.TabPage();
            this.TP_BallonTip = new System.Windows.Forms.TabPage();
            this.LV_BallonTips = new System.Windows.Forms.ListView();
            this.CH_Active = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CH_Title = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CH_WarnAtPercent = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CH_PlaySound = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CMS_BallonTips = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CMSI_AddEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.CMSI_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.TC_Settings.SuspendLayout();
            this.TP_Common.SuspendLayout();
            this.TP_BallonTip.SuspendLayout();
            this.CMS_BallonTips.SuspendLayout();
            this.SuspendLayout();
            // 
            // CB_HideAfterStart
            // 
            this.CB_HideAfterStart.AutoSize = true;
            this.CB_HideAfterStart.Location = new System.Drawing.Point(20, 6);
            this.CB_HideAfterStart.Name = "CB_HideAfterStart";
            this.CB_HideAfterStart.Size = new System.Drawing.Size(112, 17);
            this.CB_HideAfterStart.TabIndex = 0;
            this.CB_HideAfterStart.Text = "CB_HideAfterStart";
            this.CB_HideAfterStart.UseVisualStyleBackColor = true;
            // 
            // B_SaveAndClose
            // 
            this.B_SaveAndClose.AutoSize = true;
            this.B_SaveAndClose.Location = new System.Drawing.Point(10, 262);
            this.B_SaveAndClose.Name = "B_SaveAndClose";
            this.B_SaveAndClose.Size = new System.Drawing.Size(100, 23);
            this.B_SaveAndClose.TabIndex = 1;
            this.B_SaveAndClose.Text = "B_SaveAndClose";
            this.B_SaveAndClose.UseVisualStyleBackColor = true;
            this.B_SaveAndClose.Click += new System.EventHandler(this.B_SaveAndClose_Click);
            // 
            // B_Close
            // 
            this.B_Close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_Close.AutoSize = true;
            this.B_Close.Location = new System.Drawing.Point(657, 262);
            this.B_Close.Name = "B_Close";
            this.B_Close.Size = new System.Drawing.Size(75, 23);
            this.B_Close.TabIndex = 2;
            this.B_Close.Text = "B_Close";
            this.B_Close.UseVisualStyleBackColor = true;
            this.B_Close.Click += new System.EventHandler(this.B_Close_Click);
            // 
            // CB_ForceClose
            // 
            this.CB_ForceClose.AutoSize = true;
            this.CB_ForceClose.Location = new System.Drawing.Point(20, 29);
            this.CB_ForceClose.Name = "CB_ForceClose";
            this.CB_ForceClose.Size = new System.Drawing.Size(99, 17);
            this.CB_ForceClose.TabIndex = 3;
            this.CB_ForceClose.Text = "CB_ForceClose";
            this.CB_ForceClose.UseVisualStyleBackColor = true;
            // 
            // TC_Settings
            // 
            this.TC_Settings.Controls.Add(this.TP_Common);
            this.TC_Settings.Controls.Add(this.TP_BallonTip);
            this.TC_Settings.Dock = System.Windows.Forms.DockStyle.Top;
            this.TC_Settings.Location = new System.Drawing.Point(0, 0);
            this.TC_Settings.Name = "TC_Settings";
            this.TC_Settings.SelectedIndex = 0;
            this.TC_Settings.Size = new System.Drawing.Size(744, 250);
            this.TC_Settings.TabIndex = 4;
            // 
            // TP_Common
            // 
            this.TP_Common.Controls.Add(this.CB_HideAfterStart);
            this.TP_Common.Controls.Add(this.CB_ForceClose);
            this.TP_Common.Location = new System.Drawing.Point(4, 22);
            this.TP_Common.Name = "TP_Common";
            this.TP_Common.Padding = new System.Windows.Forms.Padding(3);
            this.TP_Common.Size = new System.Drawing.Size(736, 224);
            this.TP_Common.TabIndex = 0;
            this.TP_Common.Text = "TP_Common";
            this.TP_Common.UseVisualStyleBackColor = true;
            // 
            // TP_BallonTip
            // 
            this.TP_BallonTip.Controls.Add(this.LV_BallonTips);
            this.TP_BallonTip.Location = new System.Drawing.Point(4, 22);
            this.TP_BallonTip.Name = "TP_BallonTip";
            this.TP_BallonTip.Padding = new System.Windows.Forms.Padding(3);
            this.TP_BallonTip.Size = new System.Drawing.Size(736, 224);
            this.TP_BallonTip.TabIndex = 1;
            this.TP_BallonTip.Text = "TP_BallonTip";
            this.TP_BallonTip.UseVisualStyleBackColor = true;
            // 
            // LV_BallonTips
            // 
            this.LV_BallonTips.CheckBoxes = true;
            this.LV_BallonTips.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.CH_Active,
            this.CH_Title,
            this.CH_WarnAtPercent,
            this.CH_PlaySound});
            this.LV_BallonTips.ContextMenuStrip = this.CMS_BallonTips;
            this.LV_BallonTips.FullRowSelect = true;
            this.LV_BallonTips.GridLines = true;
            this.LV_BallonTips.Location = new System.Drawing.Point(6, 6);
            this.LV_BallonTips.MultiSelect = false;
            this.LV_BallonTips.Name = "LV_BallonTips";
            this.LV_BallonTips.Size = new System.Drawing.Size(722, 215);
            this.LV_BallonTips.TabIndex = 0;
            this.LV_BallonTips.UseCompatibleStateImageBehavior = false;
            this.LV_BallonTips.View = System.Windows.Forms.View.Details;
            // 
            // CH_Active
            // 
            this.CH_Active.Text = "CH_Active";
            // 
            // CH_Title
            // 
            this.CH_Title.Text = "CH_Title";
            this.CH_Title.Width = 229;
            // 
            // CH_WarnAtPercent
            // 
            this.CH_WarnAtPercent.Text = "CH_WarnAtPercent";
            // 
            // CH_PlaySound
            // 
            this.CH_PlaySound.Text = "CH_PlaySound";
            // 
            // CMS_BallonTips
            // 
            this.CMS_BallonTips.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CMSI_AddEdit,
            this.CMSI_Delete});
            this.CMS_BallonTips.Name = "CMS_BallonTips";
            this.CMS_BallonTips.Size = new System.Drawing.Size(150, 48);
            this.CMS_BallonTips.Opening += new System.ComponentModel.CancelEventHandler(this.CMS_BallonTips_Opening);
            // 
            // CMSI_AddEdit
            // 
            this.CMSI_AddEdit.Name = "CMSI_AddEdit";
            this.CMSI_AddEdit.Size = new System.Drawing.Size(149, 22);
            this.CMSI_AddEdit.Text = "CMSI_AddEdit";
            this.CMSI_AddEdit.Click += new System.EventHandler(this.CMSI_AddEdit_Click);
            // 
            // CMSI_Delete
            // 
            this.CMSI_Delete.Name = "CMSI_Delete";
            this.CMSI_Delete.Size = new System.Drawing.Size(149, 22);
            this.CMSI_Delete.Text = "CMSI_Delete";
            this.CMSI_Delete.Click += new System.EventHandler(this.CMSI_Delete_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 297);
            this.Controls.Add(this.TC_Settings);
            this.Controls.Add(this.B_Close);
            this.Controls.Add(this.B_SaveAndClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.TC_Settings.ResumeLayout(false);
            this.TP_Common.ResumeLayout(false);
            this.TP_Common.PerformLayout();
            this.TP_BallonTip.ResumeLayout(false);
            this.CMS_BallonTips.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox CB_HideAfterStart;
        private System.Windows.Forms.Button B_SaveAndClose;
        private System.Windows.Forms.Button B_Close;
        private System.Windows.Forms.CheckBox CB_ForceClose;
        private System.Windows.Forms.TabControl TC_Settings;
        private System.Windows.Forms.TabPage TP_Common;
        private System.Windows.Forms.TabPage TP_BallonTip;
        private System.Windows.Forms.ListView LV_BallonTips;
        private System.Windows.Forms.ColumnHeader CH_Title;
        private System.Windows.Forms.ColumnHeader CH_PlaySound;
        private System.Windows.Forms.ContextMenuStrip CMS_BallonTips;
        private System.Windows.Forms.ColumnHeader CH_WarnAtPercent;
        private System.Windows.Forms.ColumnHeader CH_Active;
        private System.Windows.Forms.ToolStripMenuItem CMSI_AddEdit;
        private System.Windows.Forms.ToolStripMenuItem CMSI_Delete;
    }
}