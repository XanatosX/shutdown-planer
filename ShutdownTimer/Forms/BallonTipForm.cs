﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ShutdownTimer
{
    public partial class BallonTipForm : Form
    {
        private BallonTipWarning _currentWarning;
        private bool EditMode = false;
        public BallonTipWarning CurrentWarning
        {
            get
            {
                return _currentWarning;
            }
        }
        public BallonTipForm(BallonTipWarning CurrentWarning = null)
        {
            InitializeComponent();

            if (CurrentWarning == null)
            {
                Text = Language.GetString("BallonTipForm_AddNew");
            }
            else
            {
                Text = Language.GetString("BallonTipForm_Edit");
                EditMode = true;
                _currentWarning = CurrentWarning;
            }


        }

        private void BallonTipForm_Load(object sender, EventArgs e)
        {
            if (_currentWarning != null)
            {
                TB_Title.Text = _currentWarning.Title;
                TB_Body.Text = _currentWarning.Text;
                TB_WarnAtPercent.Text = _currentWarning.WarnAtProcent.ToString();
                TB_SoundFile.Text = _currentWarning.SoundFile;
            }

            DesignForm();

        }

        private void DesignForm()
        {
            LoadLanguage();
            int NewTextboxPosition = 0;
            Label LongestLabel = this.GetLongestLabel();
            NewTextboxPosition = LongestLabel.Location.X + LongestLabel.Width + 20;
            TextBox LastEditedTextBox = null;
            foreach (Control CurrentControl in this.GetAllTopLayerControls())
            {
                if (CurrentControl.GetType() == typeof(TextBox))
                {
                    TextBox CurrentBox = (TextBox)CurrentControl;
                    CurrentBox.Location = new Point(NewTextboxPosition, CurrentBox.Location.Y);
                    LastEditedTextBox = CurrentBox;
                }
            }

            if (LastEditedTextBox != null)
                Size = new Size(LastEditedTextBox.Location.X + LastEditedTextBox.Width + 35, Height);

            B_Save.CenterControlHorizontal(this);
        }

        private void LoadLanguage()
        {
            L_Title.Text = Language.GetString("BallonTipForm_BallonTipTitle");
            L_Body.Text = Language.GetString("BallonTipForm_BallonTipBody");
            L_WarnAtPercent.Text = Language.GetString("BallonTipForm_BallonTipWarnAtPercent");
            L_SoundFile.Text = Language.GetString("BallonTipForm_BallonTipSoundFile");
            B_Save.Text = Language.GetString("Default_Save");
        }

        private void B_Save_Click(object sender, EventArgs e)
        {

            int WarnTime = 0;
            if (!int.TryParse(TB_WarnAtPercent.Text, out WarnTime))
                return;
            if (EditMode)
            {
                _currentWarning = new BallonTipWarning(WarnTime, TB_Title.Text, TB_Body.Text, ToolTipIcon.Info, TB_SoundFile.Text, _currentWarning.Active);
            }
            else
            {
                _currentWarning = new BallonTipWarning(WarnTime, TB_Title.Text, TB_Body.Text, ToolTipIcon.Info, TB_SoundFile.Text, true);
            }
            Close();
        }

        private void TB_WarnAtPercent_TextChanged(object sender, EventArgs e)
        {
            Regex RegexNumbers = new Regex("^[0-9]*$");
            if (!RegexNumbers.IsMatch(TB_WarnAtPercent.Text))
            {
                TB_WarnAtPercent.Text = TB_WarnAtPercent.Text.Remove(TB_WarnAtPercent.Text.Length - 1, 1);
                return;
            }
            int CurrentNumber = 0;
            bool Edited = false;
            if (int.TryParse(TB_WarnAtPercent.Text, out CurrentNumber))
            {
                if (CurrentNumber < 0)
                {
                    CurrentNumber = 0;
                    Edited = true;
                }
                if (CurrentNumber > 100)
                {
                    CurrentNumber = 100;
                    Edited = true;
                }
            }
            if (Edited)
                TB_WarnAtPercent.Text = CurrentNumber.ToString();

            TB_WarnAtPercent.Select(TB_WarnAtPercent.Text.Length, 0);
        }
    }
}
