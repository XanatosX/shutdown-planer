﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ShutdownTimer
{
    static class Settings
    {
        public static bool LanguageLoadet = false;
        public static bool ForceShutdown = true;
        public static bool HideAfterStart = false;
        public static int MinTimeForNotifications = 60;

        public static bool Debug = false;

        private static List<BallonTipWarning> _warnings;
        public static List<BallonTipWarning> Warnings
        {
            get
            {
                return _warnings;
            }
        }

        private static string SettingsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Shutdown planer\";
        private static string SettingsFile = SettingsDirectory+"Settings.xml";

        public static string Version;

        public static void Load()
        {
            FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
            _warnings = new List<BallonTipWarning>();
            if (versionInfo != null)
            {
                Version = versionInfo.ProductVersion;
            }
            if (!File.Exists(SettingsFile))
                Save();
            XmlSerializer Serializer = new XmlSerializer(typeof(SettingsHelper));
            using (Stream Reader = new FileStream(SettingsFile, FileMode.Open))
            {
                object Return = Serializer.Deserialize(Reader);
                if (Return.GetType() == typeof(SettingsHelper))
                {
                    SettingsHelper LoadetSettings = (SettingsHelper)Return;

                    if (LoadetSettings.LanguageString != null)
                    {
                        if (Language.switch_language(Language.GetCultureFromString(LoadetSettings.LanguageString)))
                            LanguageLoadet = true;

                        HideAfterStart = LoadetSettings.HideAfterStart;
                        MinTimeForNotifications = LoadetSettings.MinTimeForNotifications;
                        _warnings = LoadWarnings(LoadetSettings.Warnings);
                    }
                }
            }
        }

        public static void Save()
        {
            XmlSerializer Serializer = new XmlSerializer(typeof(SettingsHelper));
            using (Stream BasicWriteStream = new FileStream(SettingsFile, FileMode.Create))
            {
                SettingsHelper SettingsToSave = new SettingsHelper()
                {
                    LanguageString = Language.GetCurrentCultureCode,
                    HideAfterStart = HideAfterStart,
                    MinTimeForNotifications = MinTimeForNotifications,
                    Warnings = ConvertBallonTips(Warnings),
                };
                Serializer.Serialize(BasicWriteStream, SettingsToSave);
            }

        }

        private static List<SerializableBallonTipWarning> ConvertBallonTips(List<BallonTipWarning> Warnings)
        {
            List<SerializableBallonTipWarning> ReturnList = new List<SerializableBallonTipWarning>();
            foreach (BallonTipWarning BallonTip in Warnings)
            {
                ReturnList.Add(BallonTip.Save());
            }
            return ReturnList;
        }

        private static List<BallonTipWarning> LoadWarnings(List<SerializableBallonTipWarning> Warnings)
        {
            List<BallonTipWarning> ReturnList = new List<BallonTipWarning>();
            foreach (SerializableBallonTipWarning BallonTip in Warnings)
            {
                ReturnList.Add(BallonTip.ConvertToBallonTip());
            }
            return ReturnList;
        }

    }

    [Serializable]
    public class SettingsHelper
    {
        //public static bool LanguageLoadet;
        public string LanguageString;
        public bool HideAfterStart;
        public int MinTimeForNotifications;
        public List<SerializableBallonTipWarning> Warnings;

        public SettingsHelper()
        {
            
        }
    }
}
