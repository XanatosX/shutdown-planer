﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows.Forms;

namespace ShutdownTimer
{
    public static class Helper
    {
        public static void CenterControlHorizontal(this Control ControlToCenter, Control ParentControl)
        {
            ControlToCenter.Location = new System.Drawing.Point(ParentControl.Width / 2 - ControlToCenter.Width / 2, ControlToCenter.Location.Y);
        }

        public static Control[] GetAllTopLayerControls(this Form ControlToCheck)
        {
            if (ControlToCheck == null)
                return null;
            List<Control> ControlsFound = new List<Control>();
            foreach (Control CurrentControl in ControlToCheck.Controls)
            {
                ControlsFound.Add(CurrentControl);
            }

            return ControlsFound.ToArray();
        }

        public static Label[] GetAllTopLayerLabels(this Form ControlToCheck)
        {
            List<Label> Returnlist = new List<Label>();
            Control[] AllTopLayerControls = ControlToCheck.GetAllTopLayerControls();
            for (int i = 0; i < AllTopLayerControls.Length; i++)
            {
                Control CurrentControl = AllTopLayerControls[i];
                if (CurrentControl.GetType() == typeof(Label))
                    Returnlist.Add((Label)CurrentControl);
            }
            return Returnlist.ToArray();
        }

        public static Label GetLongestLabel(this Form ControlToCheck)
        {
            Label ReturnLabel = null;
            int CurrentLength = int.MinValue;
            foreach (Label CurrentLabel in ControlToCheck.GetAllTopLayerLabels())
            {
                if (CurrentLabel.Width > CurrentLength)
                {
                    CurrentLength = CurrentLabel.Width;
                    ReturnLabel = CurrentLabel;
                }
            }
            return ReturnLabel;
        }
    }

    public class BallonTipWarning
    {
        private bool _active;
        public bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
        private int _warnAtProcent;
        public int WarnAtProcent
        {
            get
            {
                return _warnAtProcent;
            }
        }
        public ToolTipIcon _icon;
        public ToolTipIcon Icon
        {
            get
            {
                return _icon;
            }
        }
        private string _title;
        public string Title
        {
            get
            {
                return _title;
            }

        }
        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
        }
        private bool _playSound;
        public bool PlaySound
        {
            get
            {
                return _playSound;
            }
        }
        private string _soundFile;
        public string SoundFile
        {
            get
            {
                return _soundFile;
            }
        }
        private SoundPlayer _soundPlayer;

        public BallonTipWarning(int IWarnAtProcent, string STitle, string SText, ToolTipIcon TTI_Icon, string soundFile = "", bool IsActive = true)
        {
            _warnAtProcent = IWarnAtProcent;
            _title = STitle;
            _text = SText;
            _icon = TTI_Icon;
            _soundFile = soundFile;
            if (!String.IsNullOrEmpty(_soundFile) && File.Exists(_soundFile))
            {
                _soundPlayer = new SoundPlayer(_soundFile);
                _soundPlayer.Load();
                _playSound = true;
            }
            else
            {
                _playSound = false;
            }
            _active = IsActive;
        }

        public void Play()
        {
            if (_soundPlayer != null)
                _soundPlayer.Play();
        }

        public SerializableBallonTipWarning Save()
        {
            SerializableBallonTipWarning SaveReturn = new SerializableBallonTipWarning()
            {
                Active = _active,
                WarnAtPercent = _warnAtProcent,
                Icon = _icon,
                Title = _title,
                Text = _text,
                SoundFile = _soundFile,
            };
            return SaveReturn;
        }
    }

    [Serializable]
    public class SerializableBallonTipWarning
    {
        public bool Active;
        public int WarnAtPercent;
        public ToolTipIcon Icon;
        public string Title;
        public string Text;
        public string SoundFile;

        public SerializableBallonTipWarning()
        {

        }

        public BallonTipWarning ConvertToBallonTip()
        {
            BallonTipWarning ReturnTip = new BallonTipWarning(WarnAtPercent, Title, Text, Icon, SoundFile, Active);
            return ReturnTip;
        }
    }

}
