﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using ShutdownTimer.Forms;

namespace ShutdownTimer
{
    public static class Language
    {
        private static ResourceManager res_man;
        private static CultureInfo cul;

        private static bool _initialized = false;

        private static string DefaultText = "Can't find string text";
        private static string NotInitialized = "Please Initialize first";

        private static string[] AllowedLangauges = { "en", "de"};


        public static string CurrentCulturename
        {
            get
            {
                return cul.DisplayName;
            }
        }

        public static string GetCurrentCultureCode
        {
            get
            {
                return cul.Name;
            }
        }

        public static void Initialize()
        {
            cul = CultureInfo.CurrentCulture;
            res_man = new ResourceManager("ShutdownTimer.Resources.Language.Res", typeof(MainForm).Assembly);
            if (!CheckLanguage())
                SetDefaultLanguage();
            _initialized = true;
        }

        private static bool CheckLanguage()
        {
            try
            {
                res_man.GetString("Check");
                return true;
            }
            catch (Exception)
            {
                
            }
            return false;
        }

        public static bool switch_language(CultureInfo Culture)
        {
            if (Culture == null)
                SetDefaultLanguage();
            cul = Culture;
            if (!CheckLanguage())
            {
                SetDefaultLanguage();
                return false;
            }
            return true;
        }

        public static CultureInfo GetCultureFromString(string CultureCode)
        {
            CultureInfo CurrentCulture = null;
            try
            {
                CurrentCulture = CultureInfo.GetCultureInfo(CultureCode);
            }
            catch (Exception)
            {
            }
            return CurrentCulture;
        }

        private static void SetDefaultLanguage()
        {
            cul = CultureInfo.GetCultureInfo("de");
        }

        public static List<CultureInfo> GetCultures()
        {
            List<CultureInfo> AllowedCultures = new List<CultureInfo>();
            foreach (string CultureName in AllowedLangauges)
            {
                AllowedCultures.Add(CultureInfo.GetCultureInfo(CultureName));
            }
            return AllowedCultures;
        }

        public static string GetString(string Key)
        {
            if (!_initialized)
            {
                return NotInitialized;
            }
            if (res_man == null)
                return "Can't find resource";
            string ReturnValue = "";
            try
            {
                ReturnValue = res_man.GetString(Key, cul);
            }
            catch (Exception)
            {
                return DefaultText;
            }
            
            if (ReturnValue == null)
            {
                ReturnValue = DefaultText;
            }
            return ReturnValue;
        }
    }
}
