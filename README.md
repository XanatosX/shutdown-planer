# Shutdown-Planer

## Short project description
The Shutdown-Planer is a tool used to shut down your computer after x time units. The Programm is currently under development and not stable at all so be ready to find some bugs and crashes.

## Installation

To install the Shutdown-planer download one of the realese versions and put the content of the zip folder somewhere on your pc. After you starting the programm the first time you need to set a language.

The Programm will save its settings files under "%appdata%\Shutdown planer\" the filetype which get created depends on the version of the programm. Older versions will create a ".set" file while newer version will create .xml files.

## Uninstall

To uninstall the programm simply delete all the files you unziped while installing the software. Don't forget to delete the settingsfiles in  "%appdata%\Shutdown planer\".

### System requirements

This Programm will only work on a Windows PC with .Net 4.0
